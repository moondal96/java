package com.yedam.homework;

import java.util.Scanner;

public class H20221124 { // 정보
	public static void main(String[] args) {

		// #while문 사용
		// if (menu == 1) {
		// 배열의 크기를 받는 행동만 : 잘못 넣는 실수를 방지하기 위해 2. 상품 및 가격입력할 때 배열의 크기를 확정시켜 주는 것이 좋음

		// 상품의 수와 상품명을 입력 받을 수 있도록 Scanner 등록
		Scanner sc = new Scanner(System.in);
		Product[] pd = null;// 객체, 변수의 선언은 웬만하면 반복문 밖에서 하는 게 좋음(리셋될 수도 있기 때문에)
		int productCount = 0;

		while (true) {

			System.out.println("1.상품 수 | 2.상품 및 가격입력 | 3.제품별 가격 | 4.분석 | 5.종료");
			System.out.println("입력 > ");

			String selectNo = sc.nextLine(); // 문자열로 입력 받고 비교해 보기
			// 1. 상품 수
			if (selectNo.equals("1")) {
				System.out.println("상품 수 입력 > ");
				productCount = Integer.parseInt(sc.nextLine()); // 문자열을 정수 타입으로 바꿔주기
			} else if (selectNo.equals("2")) {
				// 상품 수 입력 받은 내용을 토대로 배열 크기를 확정
				pd = new Product[productCount];
				// 만약에 productCount를 5 받았으면 pd.length가 5임
				for (int i = 0; i < pd.length; i++) {
					// 상품 객체 생성
					Product product = new Product(); // 반복문 안에서 하는 이유 : 반복문 실행할 때마다 새로운 상품을 만들기 위해서
					System.out.println((i + 1) + "번째 상품");
					// 변수에 데이터를 입력 받고 객체에 데이터를 넣는 방법
					System.out.println("상품명 > ");
					String name = sc.nextLine();
					product.ProductName = name;
					// 데이터를 입력 받음과 동시에 객체에 데이터를 넣는 방법
					System.out.println("가격 > ");
					product.price = Integer.parseInt(sc.nextLine());

					pd[i] = product; // 방에 저장
					System.out.println("====================");
				}
			} else if (selectNo.equals("3")) {
				// 배열의 크기만큼 반복문을 진행할 때 배열의 각 인덱스(방 번호)를
				// 활용하여 객체(상품)를 꺼내와서 객체(상품)에 정보를 하나씩 꺼내옴
				for (int i = 0; i < pd.length; i++) {
					String name = pd[i].ProductName; // pd[i] == product
					System.out.println("상품명 : " + name);
					System.out.println("상품 가격 : " + pd[i].price);
				}
			} else if (selectNo.equals("4")) {
				int max = pd[0].price;
				int sum = 0;
				for (int i = 0; i < pd.length; i++) {
					if (max < pd[i].price) {
						max = pd[i].price;
					}
					sum += pd[i].price; // 모든 상품들의 가격 합계
				}
				System.out.println("제일 비싼 상품 가격 : " + max);
				System.out.println("제일 비싼 상품을 제외한 상품 총 합 : " + (sum - max));
			} else if (selectNo.equals("5")) {
				System.out.println("프로그램 종료");
				break;
			}
		}
		
		// #switch문 사용해 보기
		// boolean flag = true;
		// while(true) 넣어서 진행하고 종료할 때는 false 넣어주면 됨
	}
}
