package com.yedam.chapter07;

public abstract class Culture {

	// 필드
	String title;
	int director;
	int actor;
	int audience = 0;
	int total = 0;

	// 생성자
	public Culture(String title, int director, int actor) {
		this.title = title;
		this.director = director;
		this.actor = actor;
	}

	// 메소드
	// (1) 관객수와 총점을 누적시키는 기능
	public void setTotalScore(int score) {
		// 관객 수 1씩 증가
		this.audience++;
		// 점수를 누적(총점)
		this.total += score;
	}

	// (2) 평점을 구하는 기능
	public String getGrade() {
		int avg = total / audience;

		String grade = null; // 초기화

		// #1번째 방법
		switch (avg) {
		case 1:
			grade = "☆";
			break;
		case 2:
			grade = "☆☆";
			break;
		case 3:
			grade = "☆☆☆";
			break;
		case 4:
			grade = "☆☆☆☆";
			break;
		case 5:
			grade = "☆☆☆☆☆";
			break;
		}

		// #2번째 방법
//		for(int i = 0; i < avg; i++) {
//			grade += "☆";
//		}
		
		return grade; // 반환값이 String이기 때문에 리턴값이 필요함
	}

	// (3) 정보를 출력하는 추상메소드
	public abstract void getInformation();
		
	}
