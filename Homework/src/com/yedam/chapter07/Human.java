package com.yedam.chapter07;

public class Human { // 상속을 받고 나서 자동 타입 변환으로 받을 수 있는가?
	// 필드
	String name;
	double height;
	double weight;
	
	// 생성자
	public Human(String name, double height, double weight) {
		this.name = name;
		this.height = height;
		this.weight = weight;
	}
	
	// 메소드
	public void getInformation() {
		System.out.println("이름 : " + name + "키 : "  + height + "몸무게 : " + weight);
	}
}
