package com.yedam.chapter07;

public class Movie extends Culture{

	String gerne;
	
	public Movie(String title, int director, int actor, String gerne) {
		super(title, director, actor);
		this.gerne = gerne;
	}
	
	@Override
	public void getInformation() {
		System.out.println("영화제목 : " + title);
		System.out.println("감독 : " + director);
		System.out.println("배우 : " + actor);
		System.out.println("총점 : " + total);
		System.out.println("영화평점 : " + getGrade());
	}

}
