package com.yedam.chapter07;

public class EmpDept extends Employee{
	
	// 필드
	public String deptName;
	
	// 생성자
	public EmpDept(String name, int salary, String deptName) {
		super(name, salary); // 부모 클래스에 있는 생성자를 호출
		this.deptName = deptName;
	}
	
	// 메소드
	@Override // 우클릭으로 만들기
	public void getInformation() {
		super.getInformation();
		System.out.println(" 부서 : " + deptName); // 추가된 것만 넣어주면 됨
	}

	@Override
	public void print() {
		super.print();
		System.out.println("서브클래스");
	}
}
