package com.yedam.chapter07;

public class StandardWeightInfo extends Human {

	public StandardWeightInfo(String name, double height, double weight) {
		super(name, height, weight);
	} // 필드를 딱히 사용하지 않고 만들어

	// 메소드
	public void getInformation() {
		super.getInformation();
		// #1번 방식
		//System.out.println("표준체중 : " + getStandardWeight());
		// #2번 방식
		System.out.printf("표준체중 %.1f 입니다.\n", getStandardWeight());
	}

	// (2) public double getStandardWeight() : 표준체중을 구하는 기능
	// ( * 표준 체중 : (Height - 100) * 0.9 )
	public double getStandardWeight() {
		double sw = (height - 100) * 0.9;
		return sw; // 결과가 실수값으로 돌아감
	}

}
