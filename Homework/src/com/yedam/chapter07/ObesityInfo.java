package com.yedam.chapter07;

public class ObesityInfo extends StandardWeightInfo {

	public ObesityInfo(String name, double height, double weight) {
		super(name, height, weight);
	}

	public void getInformain() {
		// 비만도
		double bmi = getObesity();
		String obesity = null;

		if (bmi <= 18.5) {
			obesity = "저체중";
		} else if (bmi < 22.9) {
			obesity = "정상";
		} else if (bmi <= 24.9) {
			obesity = "과체중";
		} else {
			obesity = "비만";
		}

		System.out.println("이름 : " + name + "키 : " + height + "몸무게 : " + weight);
	}

	public double getObesity() {
		double bmi = (weight - getStandardWeight()) / getStandardWeight() * 100;
		return bmi;
	}

}
