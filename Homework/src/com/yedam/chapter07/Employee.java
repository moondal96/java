package com.yedam.chapter07;

public class Employee {
	// 필드
	protected String name; // protected : 상속의 느낌~
	protected int salary;

	// 생성자
	public Employee(String name, int salary) {
		this.name = name;
		this.salary = salary; // setter를 통해 데이터 초기화
	}

	// 메소드
	public void getInformation() {
		System.out.print("이름 : " + name + " 연봉 : " + salary);
	}

	public void print() {
		System.out.println("수퍼클래스");
	}

	public String getName() {
		return name;
	}

	public int getSalary() {
		return salary;
	} // getter를 통해 데이터 가져오기
}
