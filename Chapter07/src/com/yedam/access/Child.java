package com.yedam.access;

import com.yedam.inheri.Parent; // #2 근데 import 사용하면 상속 받을 수 있음

public class Child extends Parent { // #1 원래는 다른 클래스라서 상속 못 받음
	public String lastName;
	public int age;

	public Child() {

	}

	public void showInfo() {
		System.out.println("내 성씨 : " + firstName);
		System.out.println("DNA : " + DNA);
	}

}
