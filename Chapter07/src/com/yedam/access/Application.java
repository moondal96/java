package com.yedam.access;

// import com.yedam.inheri.Child; >> 다른 클래스를 불러오기 때문에 지워줘야 함

public class Application {
	public static void main(String[] args) {
		Child child = new Child();

		child.lastName = "또치";
		child.age = 20;

//	System.out.println("내 이름 : " + child.firstName + child.lastName); // fisrtName 정의 안 했는데도 괜찮음
//	System.out.println("DNA : " + child.DNA);

		child.showInfo();
		System.out.println("나이 : " + child.age);
	}
}
