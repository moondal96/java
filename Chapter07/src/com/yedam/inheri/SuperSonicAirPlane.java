package com.yedam.inheri;

public class SuperSonicAirPlane extends AirPlane {
	// 필드
	// 일반 비행
	public static final int NORMAR = 1; // static final => 불변의 상수 만들기

	// 초음속 비행
	public static final int SUPERSONIC = 2;

	public int flyMode = NORMAR;

	// 생성자

	// 메소드
	@Override
	public void fly() {
		if (flyMode == SUPERSONIC) {
			System.out.println("초음속 비행 모드");
		} else {
			super.fly(); // 부모가 가진 fly를 실행
		}
	}
}
