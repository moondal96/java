package com.yedam.inheri;

public class DmbCellphone extends Cellphone{
	// 필드
	int channel; 
	
	// 생성자
	public DmbCellphone(String model, String color, int channel) {
		this.model = model;
		this.color = color;
		this.channel = channel;
	}
	
	// 메소드
	void turnOnDmb() {
		System.out.println("채널 " + channel + "번 방송 수신");
	}
	
	void turnOffDmb() {
		System.out.println("방송을 멈춤");
	}
	
	

}
