package com.yedam.inheri;

public class personExample {
	public static void main(String[] args) {
		Person p1 = new Person("고희동", "123456-1234567");
		
		// #부모 꺼
		// 부모 클래스의 필드 호출
		// Person()을 실행할 때, 부모 객체를 만들어서
		// 그 안에 있는 인스턴스 필드를 가져와서 실행
		System.out.println("name : " + p1.name);
		System.out.println("ssn : " + p1.ssn);
		
		// #자식 꺼
		// Person()을 실행할 때, 자신이 가지고 있는 인스턴스 필드
		// 호출하여 실행
		System.out.println("age : " + p1.age);
		
	}
}
