package com.yedam.inheri;

public class Child  extends Parent { // 클래스 이름에다가 누구를 연결할 건지 적음
	// 부모 클래스를 객체로 만들지 않고 물려 받을 수 있음
	public String lastName;
	public int age;

	// 메소드
	// 오버라이딩 예제

	@Override // 오버라이딩 미리 선언. 우클릭으로도 생성 가능
	public void method1() {
		System.out.println("child class -> method1 Override");
	}
	
	public void method3() {
		System.out.println("child class -> method3 Call");
	}
}