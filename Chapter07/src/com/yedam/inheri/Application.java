package com.yedam.inheri;

public class Application {
	public static void main(String[] args) {
		// #자식1
		Child child = new Child();
		
		child.lastName = "또치";
		child.age = 20;
		
		System.out.println("내 이름 : " + child.firstName + child.lastName); // fisrtName 정의 안 했는데도 괜찮음
		System.out.println("DNA : " + child.DNA);
		// Parent 클래스 -> bloodType을 private으로 설정했기 때문에
		// Child 클래스 -> Parent 클래스의 bloodType 사용 못함
		// System.out.println("혈액형 : " + child.bloodType);
		System.out.println("나이 : " + child.age);
		
		// #자식2
		Child2 child2 = new Child2();
		
		child2.lastName = "희동";
		child2.age = 5;
		child2.bloodType = 'A';
		
		System.out.println("내 이름 : " + child2.firstName + child2.lastName); // fisrtName 정의 안 했는데도 괜찮음
		System.out.println("DNA : " + child2.DNA);
		// Child에 존재하는 bloodType은 상관 없음
		System.out.println("혈액형 : " + child2.bloodType);
		System.out.println("나이 : " + child2.age);
	}
}
