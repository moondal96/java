package com.yedam.inheri;
public class DmbphoneExample {
	public static void main(String[] args) { // 실행을 하기 위해서 main문이 필요한 것임
		DmbCellphone dmb = new DmbCellphone("자바폰", "검정", 10);
		
		// 부모 클래스의 필드 호출
		System.out.println(dmb.model);
		System.out.println(dmb.color);
		
		// 자식 클래스의 필드 호출
		System.out.println(dmb.channel);
		
		// 부모 클래스의 메소드 호출
		dmb.powerOn();
		dmb.bell();
		dmb.hangUp();
		
		// 자식 클래스의 메소드 호출
		dmb.turnOnDmb();
		dmb.turnOffDmb();
		
		// 부모 클래스의 메소드 호출
		dmb.powerOff();
	}
}
