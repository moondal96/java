package com.yedam.inheri;

public class AirplaneExample {

	public static void main(String[] args) {
		SuperSonicAirPlane sa = new SuperSonicAirPlane();
		
		sa.takeOff();
		
		sa.fly(); // 일반으로 가다가
		
		sa.flyMode = SuperSonicAirPlane.SUPERSONIC; // 비행 모드 바꾸고
		
		sa.fly(); // 초음속이 나올 거여
		
		sa.flyMode = SuperSonicAirPlane.NORMAR;
		
		sa.fly();
		
		sa.land();
	}

}
