package com.yedam.inheri;

public class OverrideExam {
	public static void main(String[] args) {
		Child child = new Child();

		child.method1(); // 오버라이딩한 내용이 출력됨

		child.method2();

		child.method3();
	}
}
