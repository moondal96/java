package com.yedam.poly;

public class Child extends Parent {

	// #클래스 간의 강제 타입 변환
	public String field2;

	@Override
	public void method2() {
		System.out.println("child-method2()");
	}

	public void method3() {
		System.out.println("child-method3()");
	}

	@Override
	public void method4() {
		System.out.println("나는 손자!");
	}

}
