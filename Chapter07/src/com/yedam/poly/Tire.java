package com.yedam.poly;

public class Tire { // 부모 클래스
	// 타이어가 수명이 다 되면 자식 클래스(다른 타이어)로 바꾸겠다
	// 대신 바꿔 끼울 때 차마다의 규격이 있다고 생각해야 함

	// 필드
	public int maxRotation; // 최대 회전수(타이어 수명)
	public int accRotation; // 누적 회전수(현재까지의 회전수)
	public String location; // 타이어 위치

	// 생성자
	public Tire(String location, int maxRotation) {
		this.location = location;
		this.maxRotation = maxRotation;

	}

	// 메소드
	// 타이어가 굴러가는 메소드
	public boolean roll() { // roll이 실행될 때 한번 굴러간다는 의미
		++accRotation;
		if (accRotation < maxRotation) { // 수명이 남았을 때
			System.out.println(location + "Tire 수명 : " + (maxRotation - accRotation) + "회");
			return true;
		} else {
			System.out.println("###" + location + "Tire 펑크" + "###");
			return false;
		}
	}

}
