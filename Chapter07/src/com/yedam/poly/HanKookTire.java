package com.yedam.poly;

public class HanKookTire extends Tire{ // 오류 클릭
	
	public HanKookTire(String location, int maxRotation) {
		super(location, maxRotation);
	}
	
	@Override
	public boolean roll() { // roll이 실행될 때 한번 굴러간다는 의미
		++accRotation;
		if (accRotation < maxRotation) { // 수명이 남았을 때
			System.out.println(location + "HanKook Tire 수명 : " + (maxRotation - accRotation) + "회");
			return true;
		} else {
			System.out.println("###" + location + "HanKook Tire 펑크" + "###");
			return false;
		}
	}

}
