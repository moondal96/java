package com.yedam.poly;

public class Parent extends GrandParent{

	// #클래스 간의 강제 타입 변환
	public String field;

	public void method1() {
		System.out.println("parent-method1");
	}

	public void method2() {
		System.out.println("parent-method2");
	}

}