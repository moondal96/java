package com.yedam.poly;

public class DriverExample {
	public static void main(String[] args) {
		// 부모 : Vehicle - > 자식 : Bus, Taxi
		// Driver -> Vehicle 매개 변수로 하는 drive
		// drive(Vehicle vehicle) <- 매개 변수에 자식 클래스를 대입
		
		Vehicle vehicle = new Vehicle();
		
		Driver driver = new Driver();
		Bus bus = new Bus();
		//driver.drive(new Bus());
		driver.drive(bus);
		
		driver.drive(new Taxi());
	}
}
