package com.yedam.poly;

public class ChildExample {
	public static void main(String[] args) {
//		Child child = new Child();

		// ★★★클래스 간의 자동 타입 변환★★★
		// 부모 클래스에 있는 메소드를 사용하되
		// 단, 자식 클래스에 재정의가 되어 있으면
		// 자식 클래스에 재정의된 메스드를 사용하겠다
//		Parent parent = child;
//		
//		parent.method1();
//		parent.method2();
		// parent.method3();

		// #클래스 간의 강제 타입 변환
		// 자동타입변환으로 인해서 자식클래스 내부에 정의된 필도, 메소드를 못 쓸 경우
		// 강제타입변환을 함으로써 자식클래스 내우베 정의된 필드와 메소드를 사용
		Parent parent = new Child();

		parent.field = "data1";
		parent.method1();
		parent.method2();
//		parent.field2 = "data2";
//		parent.method3(); // 안됨

		Child child = (Child) parent;
		child.field2 = "date2";
		child.method3();
		child.method2();
		child.method1();
		child.field2 = "data";

		// #클래스 타입 확인 예제
		method1(new Parent()); // 자기 자신으로 만든 객체가 만들어질 뿐임 > 엄연히 다른 클래스
		method1(new Child()); // 1. 자동 타입 변환 발생 2. 변환 성공

		// #자동 타입 변환
		GrandParent gp = new Child();
		gp.method4();
	}

	// #클래스 타입 확인 예제
	public static void method1(Parent parent) {
		if (parent instanceof Child) {
			Child child = (Child) parent;
			System.out.println("변환 성공");
		} else {
			System.out.println("변환 실패");
		}
	}

}
