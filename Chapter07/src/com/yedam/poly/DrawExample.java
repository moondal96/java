package com.yedam.poly;

public class DrawExample {
	public static void main(String[] args) {
		// 자동 타입 변환
		// 부모클래스 변수 = new 자식클래스()
		
		// 다형성을 구현
		// 
		Draw figure = new Circle(); // 하나의 객체로 여러 모습을 보여주는 것 : 다형성
		
		figure.x = 1;
		figure.y = 2;
		figure.draw(); // 자식이 재정의한 뭔가가 있으면 자식의 것이 실행됨
		
		figure = new Rectangle();
		
		figure.draw(); 
	}
}
