package com.yedam.abs;

public class Cat extends Animal { // #추상 메소드

	public Cat() {
		this.kind = "표유류";
	}

	@Override
	public void sound() {
		System.out.println("야옹");
	}
}
