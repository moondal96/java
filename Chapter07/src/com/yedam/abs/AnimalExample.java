package com.yedam.abs;

public class AnimalExample {
	public static void main(String[] args) {
		Cat cat = new Cat(); // 객체 만들기
		cat.sound();

		System.out.println();

		Animal animal = new Cat();
		animal.sound();
		animal.breathe();
		System.out.println(animal.kind);

		animalSound(new Cat());

		// 추상클래스 -> 클래스와 별반 다른 게 없지만
		// 차이점
		// 1) 자기 자신을 객체로 만들지 못함
		// 2) 추상 메소드가 존재하며, 상속을 받게 되면 추상 메소드는 반드시 구현해야 함
		// 3) 스스로 객체(인스턴스)화가 안 되므로 자식 클래스를 통한 자동타입변환으로 구현함
	}

	// #매개 변수를 활용한 자동타입변환
	public static void animalSound(Animal animal) {
		animal.sound();
	}
}
