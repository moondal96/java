package com.yedam.arrange;

public enum Week { // class가 아니라 enum으로 만듦!!
	MONDAY,
	TUESDAY,
	WEDNESDAY,
	THURSDAY,
	FRIDAY,
	SATURDAY,
	SUNDAY
}
