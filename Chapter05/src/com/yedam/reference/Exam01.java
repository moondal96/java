package com.yedam.reference;

public class Exam01 {
	public static void main(String[] args) {
		int intVal = 10;
		//array : 배열 => 참조 타입
		//int[] array = new int[6];
		int[] array = {1,2,3,4,5,6};
		int[] array2 = {1,2,3,4,5,6};
		
		System.out.println(intVal);
		System.out.println(array); //힙에 들어가 있는 array의 주소값
		
		System.out.println(array2);
		System.out.println(array == array2); //다른 주소값
		
		int[] array3; //null 상태 > 주소가 없는 상태
		int a; //초기값이 없는 상태
		
		//System.out.println(array3);
		//System.out.println(a);
	}
}