package com.yedam.array;

public class Exam01 {
	public static void main(String[] args) {
		
		int[] intArry = {1,2,3,4,5,6};
		//아래 과정을 한 번에 수행한 것임
		//int[] intArry = new int[6];
		//intArry[0] = 1 //첫 번째 데이터가 0번 째에 들어가 있다
		//intArry[1] = 2
		
		
		//#1 이름이랑 메모리 같이 할당하겠다
		String[] strArry = new String[10]; //String 타입의 집을 10개 짓겠다

		//#2 이름만 만들어 놓고 나중에 메모리 할당하겠다
		int[] intArry2;
		intArry2 = new int[5];
		
		
		//1. 배열 만들자마자 데이터 넣기
		int[] scores = {83,90,87}; 
		
		System.out.println("scores 첫번째 값 : " + scores[0]);
		System.out.println("scores 세번째 값 : " + scores[2]);
		
		//반복문과 배열
		int sum = 0;
		
		for(int i = 0; i < 3; i++) {
			System.out.println(scores[i]);
			sum = sum + scores[i];
		}
		System.out.println("총 합계 : " + sum);
		
		
		//2. new 연산자를 활용해서 배열 만들기
		//1)
		int[] point;
		point = new int[] {83,90,87};
		
		sum = 0;
		for(int i = 0; i < 3; i++) {
			System.out.println(point[i]);
			sum = sum + point[i];
		}
		System.out.println("총 합계 : " + sum);
		
		//2)
		int[] arr1 = new int[3];
		for(int i = 0; i < 3; i++) {
			System.out.println("arry1["+i+"] : " + arr1[i]);
		} //참조 타입만 가능
		
		//데이터 넣기
		arr1[0] = 10;
		arr1[1] = 20;
		arr1[2] = 30;
		
		for(int i = 0; i < 3; i++) {
			System.out.println("arry1["+i+"] : " + arr1[i]);
		}
		
		//기본 타입(실수형) 배열
		double[] arr2 = new double[3];
		for(int i = 0; i < 3; i++) {
			System.out.println("arr2["+i+"] : " + arr2[i]);
		}
		
		arr2[0] = 0.1;
		arr2[1] = 0.2;
		arr2[2] = 0.3;
		
		for(int i = 0; i < 3; i++) {
			System.out.println("arr2["+i+"] : " + arr2[i]);
		}
		
		//참조 타입(String) 배열
		String[] arr3 = new String[3];
		for(int i = 0; i < 3; i++) {
			System.out.println("arr3["+i+"] : " + arr3[i]);
		} //null
		
		arr3[0] = "3월";
		arr3[1] = "4월";
		arr3[2] = "5월";
		
		for(int i = 0; i < 3; i++) {
			System.out.println("arr3["+i+"] : " + arr3[i]);
		}
	}
}
