package com.yedam.array;

public class Exam08 {
	public static void main(String[] args) {
		String[] strArray = new String[3];

		// strArray[0] = "yedam"; >> 이거랑
		// String str = "yedam";
		// strArray[0] = str; >> 이거랑 동일

		// strArray[0] == str == "yedam"; >> 삼단논법
		strArray[0] = "yedam";
		strArray[1] = "yedam";
		strArray[2] = new String("yedam");

		System.out.println(strArray[0] == strArray[1]); // true
		System.out.println(strArray[0] == strArray[2]); // 새로운 집을 지었기 때문
		System.out.println(strArray[0].equals(strArray[2])); // true

		// 배열 복사
		// #1 for문
		int[] oldArray = { 1, 2, 3 };

		int[] newArray = new int[5];

		for (int i = 0; i < oldArray.length; i++) {
			newArray[i] = oldArray[i];
		}

		for (int i = 0; i < newArray.length; i++) {
			System.out.println(newArray[i]);
		}

		// #2 메소드
		int[] oldArray2 = { 1, 2, 3, 4, 5, 6, 7 };

		int[] newArray2 = new int[10]; // 배열 갯수가 더 적으면 안됨

		System.arraycopy(oldArray2, 0, newArray2, 0, oldArray2.length);

		for (int i = 0; i < newArray2.length; i++) {
			System.out.println(newArray2[i]);
		}
		
		
		// 향상된 for문
		for(int temp : newArray2) { // newArray2 값이 temp에 저장됨
			System.out.print(temp + "\t");
		}
		
		
		
		
		
		
		
		
	}
}
