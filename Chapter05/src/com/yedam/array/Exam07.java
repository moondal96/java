package com.yedam.array;

public class Exam07 {
	public static void main(String[] args) {
		int[][] intAry = { { 1, 2 }, { 1, 2, 3 } };

		int[][] mathScore = new int[2][3]; // {{1,2,3},{1,2,3}}

		for (int i = 0; i < mathScore.length; i++) { // 첫번째 문(배열)을 여는 배열
			System.out.println(mathScore.length); // 2
			for (int k = 0; k < mathScore[i].length; k++) { // 두번째 문(배열)을 여는 배열
				System.out.println("mathScore[" + i + "][" + k + "]=" + mathScore[i][k]);
			}
		}
	}
}
