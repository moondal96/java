package com.yedam.array;

import java.util.Scanner;

public class Exam05 { // 201p
	public static void main(String[] args) {
		boolean run = true;
		int studentNum = 0;
		int[] scores = null;
		Scanner scanner = new Scanner(System.in);

		while (run) {
			System.out.println("--------------------------------------------");
			System.out.println("1.학생수 | 2.점수입력 | 3.점수리스트 | 4.분석 | 5.종료");
			System.out.println("--------------------------------------------");
			System.out.println("선택> ");

			int selectNo = Integer.parseInt(scanner.nextLine());

			if (selectNo == 1) {
				System.out.println("학생수> ");
				studentNum = Integer.parseInt(scanner.nextLine()); // 배열 크기 설정 완료
			} else if (selectNo == 2) {
				scores = new int[studentNum]; // 학생 수 잘못 설정했을 때 방지
				for (int i = 0; i < scores.length; i++) {
					System.out.println("scores[" + i + "] >");
					scores[i] = Integer.parseInt(scanner.nextLine());
				}
			} else if (selectNo == 3) {
				for (int i = 0; i < scores.length; i++) {
					System.out.println("scores[" + i + "] >" + scores[i]);
				}
			} else if (selectNo == 4) {
				int sum = 0;
				int max = scores[0];
				for (int i = 0; i < scores.length; i++) {
					if (max < scores[i]) {
						max = scores[i];
					}
					sum = sum + scores[i];
				}

				System.out.println("최고 점수: " + max);
				System.out.println("평균 점수: " + (double) sum / scores.length);
			} else if (selectNo == 5) {
				run = false;
			}
		}
		System.out.println("프로그램 종료");
	}
}
