package com.yedam.array;

import java.util.Scanner;

public class Exam03 { // 가변적으로 배열을 바꾸는 방법
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		int[] ary;
		int no;

		System.out.println("배열의 크기>");
		no = Integer.parseInt(sc.nextLine());

		ary = new int[no]; // 최초의 한 번만

		System.out.println(ary.length);

		// 배열의 크기만큼 데이터를 넣는 것
		for (int i = 0; i < ary.length; i++) {
			System.out.println("입력>");
			ary[i] = Integer.parseInt(sc.nextLine());
		}

	}
}
