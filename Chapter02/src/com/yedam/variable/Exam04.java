package com.yedam.variable;

public class Exam04 {
	public static void main(String[] args) {
		byte bVal = 10;
		byte bVal2 = 20;
		
		//int 아래 데이터 타입일 경우, 연산하게 되면
		//int로 결과값이 나옴
		int iResult = bVal + bVal2;
		System.out.println("Byte간 연산 결과(int) : " + iResult);
		
		char cVal = 'A';
		char cVal2 = 10;
		iResult = cVal + cVal2;
		System.out.println("Char간 연산 결과(int) : " + iResult);
		System.out.println("Char간 연산 결과(문자) : " + (char)iResult);
		
		int iVal2 = 30;
		double dVal3 = iVal2/4.0; //실수 나누기 정수로 받을 수 없음
		System.out.println("int/실수 : " + dVal3);
		
		int x = 20;
		int y = 9;
		double result = (double)x/y; //double을 없애게 되면 정수 연산이 됨
		System.out.println("double 자동 변환 : " + result);
		
		
		//+ 연산에서의 문자열 자동 타입 변환
		int value = 10 + 2 + 8;
		System.out.println("Value : " + value); //20
		
		String str1 = 10+2+"8";
		System.out.println("str1 : " + str1); //128
		
		String str2 = 10+"2"+8;
		System.out.println("str2 : " + str2); //1028

		String str3 = "10"+2+8;
		System.out.println("str3 : " + str3); //1028
		
		String str4 = "10"+(2+8);
		System.out.println("str4 : " + str4); //1010
		
		
		//문자열 강제 타입 변환
		String iNo = "10";
		String bNo = "8";
		String dNo = "3.222";
		
		//1)
		int sNo = Integer.parseInt(iNo);
		byte sNo2 = Byte.parseByte(bNo);
		double sNo3 = Double.parseDouble(dNo);
		
		System.out.println("int 변환 : " + sNo + "\n byte 변환 : " + sNo2 + "\n double 변환 :" + sNo3);
		
		//2)
		String str5 = String.valueOf(sNo); 
		String str6 = String.valueOf(sNo2);
		String str7 = String.valueOf(sNo3);
		
		System.out.println(String.valueOf(sNo) + String.valueOf(sNo2)); //어떤 데이터 타입을 넣든 String 타입으로
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	}
}
