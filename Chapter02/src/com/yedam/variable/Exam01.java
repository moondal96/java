package com.yedam.variable;

public class Exam01 {
	
	public static void main(String[] args) {
//		int value;
//		value = 10;
//		System.out.println(value);
//		
//		
//		//변수 값 복사
//		int x = 3;
//		int y = 5;
//		int temp; //temp -> 임시로 저장
//		
//		//데이터 변경 전
//		System.out.println("x : " + x + ", " + "y : " +  y);
//
//		temp = x;
//		x = y;
//		y = temp;
//		
//		//데이터 변경 후
//		System.out.println("x : " + x + ", " + "y : " +  y);
		
		
		//로컬 변수
		int v1 = 15;
		
		if(v1 > 10) {
			int v2;
			v2 = v1 - 10;
		}
		//int v3 = v1 + v2 + 5; //v2가 범위 밖이라서 실행이 안 됨
	}
}
