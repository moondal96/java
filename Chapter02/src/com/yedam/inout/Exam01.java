package com.yedam.inout;

import java.io.IOException;
import java.util.Scanner;

public class Exam01 {
	public static void main(String[] args) throws Exception {
		int value = 123;
		String name = "상품";
		double price = 1000.10;
		System.out.printf("상품의 가격 : %d\n", value);
		System.out.printf("%s의 가격 : %d\n", name, value);
		System.out.printf("%s의 가격 : %d원, %f\n", name, value, price);
		
		
		//1) 정수 사용
		value = 11;
		System.out.printf("%d\n", value); //11
		System.out.printf("%6d\n", value); //____11
		System.out.printf("%-6d\n", value); //11____
		System.out.printf("%06d\n", value); //000011
		System.out.println();
		
		//2) 실수 사용
		price = 123.4567;
		System.out.printf("%f\n", price); //123.456700(반올림)
		System.out.printf("%f\n", (double)value/5);
		System.out.printf("%10.2f\n", price); //____123.46
		System.out.printf("%-10.2f\n", price); //123.46____
		System.out.printf("%010.2f\n", price); //000123.46
		System.out.println();
		
		//3) 문자열 사용
		System.out.printf("%s\n", "문자열사용"); //문자열사용
		System.out.printf("%6s\n", "문자열사용"); //_문자열사용
		System.out.printf("%-6s\n", "문자열사용"); //문자열사용_
		
		//기본 출력문
		System.out.print("아무것도 없는 print"); 
		System.out.println();
		
		//입력(93p)
//		int keyCode;
//		System.out.println("원하는 값 입력>");
//		keyCode = System.in.read();
//		System.out.println("keyCode : " + keyCode);
		
		//스캐너(Scanner) : 통째로 외우기
		Scanner sc = new Scanner(System.in);
		System.out.println("데이터 입력>");
		String word = sc.nextLine(); //문자열 입력(공백 포함)
		System.out.println(word);
		
		if(word.equals("test")) {
			System.out.println("equal : 입력하신 문자열과 비교 문자열이 같음");
		}
		if(word == "test") {
			System.out.println("== : 입력하신 문자열과 비교 문자열이 같음");
		}
	}
}
