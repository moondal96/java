package com.yedam.loop;

public class Exam04 {
	public static void main(String[] args) {
		while(true) { //while문에서의 true => 계속 반복해라
			int num = (int)(Math.random() * 6) + 1;
			System.out.println(num);
			if(num == 6) {
				break;
			}
		}
		System.out.println("end of prog");
		
		for(int i=0; i<=10; i++) {
			for(int j=0; j<=10; j++) {
			if(i+j == 4) {
				System.out.println("i + j = " + (i+j));
				break; //로컬 변수만 해당
			}
		}
	}
		
		
		//바깥쪽 반복문 종료
		Outter : for(char upper = 'A'; upper <= 'z'; upper++) {
			for (char lower = 'a'; lower <= 'z'; lower++) {
				System.out.println(upper + "-" + lower);
				if(lower == 'g') {
					break Outter;
				}
			}
		}
		System.out.println("end of prog");
		
		
		for(int i=0; i<=10; i++) {
			if(i%2 == 0) {
				continue; // 아래 있는 건 신경 안 쓰고 위로 올라감
			}
			System.out.println(i);
		}
		
		
		
		
		
		
	}
}
