package com.yedam.oop;

public class SmartPhone { // **설계도**

	// 1. 필드
	// 객체의 정보를 저장
	String name;
	String maker;
	int price;

	// 2. 생성자 : 클래스 이름이랑 동일해야
	// 1) 2) 3) 생성자를 사용할 때 메소드를 호출할 수 있음
	// 기본 생성자
	public SmartPhone() {

	}

	// 매개 변수가 1개인 생성자
	public SmartPhone(String name) {
		// 객체를 만들 때 내가 원하는 행동 or 데이터 저장 등
		// 할 때, 여기에 내용을 구현
	}

	public SmartPhone(int price) {

	}

	// 매개 변수가 2개인 생성자
	public SmartPhone(String name, int price) {

	}

	// 매개 변수 생성자 (3개)
	public SmartPhone(String name, String maker, int price) { // 객체 초기화하는 행동
		this.name = name; // this => 클래스 내부에서 데이터를 가져올 때 사용, 나 자신(내가 보고 있는 페이지)
		this.maker = maker;
		this.price = price;
	}

	// 3. 메소드
	// 객체의 기능을 정의
	void call() {
		System.out.println(name + "전화를 겁니다.");
	}

	void hangUp() {
		System.out.println(name + "전화를 끊습니다.");
	}

	// #1 리턴 타입이 없는 경우 : void
	void getInfo(int no) {
		System.out.println("매개 변수 : " + no);

	}

	// #2 리턴 타입이 있는 경우
	// 1) 기본 타입 : int, double, long.....
	// 2) 참조 타입 : String, 배열, 클래스.....
	// 2-1) 리턴 타입이 기본 타입일 경우
	int getInfo(String temp) {

		return 0;
	}

	// 2-2) 리턴 타입이 참조 타입일 경우
	String[] getInfo(String[] temp) {

		return temp;
	}

}
