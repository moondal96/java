package com.yedam.oop;

public class Application4 { // 정보
	public static void main(String[] args) {
		
		// #매개변수 존재
		Student Info1 = new Student("고길동", "예담고등학교", 221124, 10, 20, 30);
		
		// #매개변수 미존재
		Student Info2 = new Student();
		Info2.name = "김둘리";
		Info2.school = "예담고등학교";
		Info2.num = 221125;
		Info2.kor = 70;
		Info2.math = 80;
		Info2.eng = 90;
		Info2.getInfo();
		System.out.println();
		
		// #매개변수 존재
		Student Info3 = new Student("김또치", "예담고등학교", 221126, 40, 50, 60);

		
		Info1.getInfo();
		System.out.println();
		
		Info2.getInfo();
		System.out.println();
		
		Info3.getInfo();
		System.out.println();
		
	}

}




