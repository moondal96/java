package com.yedam.oop;

public class Application3 { // 정보
	public static void main(String[] args) {
		Book study1 = new Book("혼자서 공부하는 자바", "학습서", 24000, "한빛미디어", "yedam-001");
		Book study2 = new Book("이것이 리눅스다", "학습서", 32000, "한빛미디어", "yedam-002");
		Book study3 = new Book("자바스크립트 파워북", "학습서", 22000, "어포스트", "yedam-003");

		study1.getInfo();
		System.out.println();
		study2.getInfo();
		System.out.println();
		study3.getInfo();
	}
}