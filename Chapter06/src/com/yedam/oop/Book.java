package com.yedam.oop;

public class Book { // 설계도

	// 필드
	String name;
	String type; // String kind = "학습서" > 중복되는 거는 미리 지정해도 됨
	int price;
	String publi;
	String isbn;

	// 생성자
	public Book(String name, String type, int price, String publi, String isbn) {
		this.name = name;
		this.type = type;
		this.price = price;
		this.publi = publi;
		this.isbn = isbn;
	}

	// 메소드
	void getInfo() {
		System.out.println("책 이름 : " + name);
		System.out.println("# 내용");
		System.out.println("1) 종류 : " + type);
		System.out.println("2) 가격 : " + price);
		System.out.println("3) 출판사 : " + publi);
		System.out.println("4) 도서번호 : " + isbn);
	}
}


// 필드를 5개 만든다 > 생성자를 통해 데이터를 받아준다 > 
//문제
//*  위 내용을 출력할 수 있도록 Book 클래스를 생성하고 내용을 출력 할수 있도록
//*  getInfo() 메소드를 추가하세요. 
//*  메인 클래스에서 각각의 객체를 생성하고 getInfo()로 객체의 속성을 출력하시오.