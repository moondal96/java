package com.yedam.oop;

public class Application2 {
	public static void main(String[] args) {
		Calculator cl = new Calculator();

		int sumResult = cl.sum(1, 2);

		double subResult = cl.sub(10, 20);

		System.out.println(sumResult);
		System.out.println(subResult);

		// #리턴 값으로 하는 법
		// String temp = cl.result("메소드 연습");
		// System.out.println(temp);
		// System.out.println(cl.result("메소드 연습")); // 위에 두 개 합친 것

		// #리턴 값 없이 하는 법
		cl.result("메소드 연습");

		Computer myCom = new Computer(); // 
		int result = myCom.sum(1, 2, 3);
		System.out.println(result);
		result = myCom.sum(1, 2, 3, 4, 5, 6);
		System.out.println(result);

	}
}
