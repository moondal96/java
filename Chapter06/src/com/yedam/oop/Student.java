package com.yedam.oop;

public class Student { // 설계도

	// 필드
	String name;
	String school;
	int num;
	int kor;
	int math;
	int eng;

	// 생성자
	// 1) 생성자를 통한 필드 초기화 -> 생성자 (매개변수) : 참고하는 정보에 매개 변수가 있을 때
	public Student(String name, String school, int num, int kor, int math, int eng) {
		this.name = name;
		this.school = school;
		this.num = num;
		this.kor = kor;
		this.math = math;
		this.eng = eng;

	}

	// 2) 객체 필드에 접근하여 필드 초기화 -> 기본 생성자 : 참고하는 정보에 매개 변수가 없을 때
	public Student() {

	}

	// 메소드
	void getInfo() {
		// sum/avg/double
		System.out.println("학생의 이름 : " + name);
		System.out.println("학생의 학교 : " + school);
		System.out.println("학생의 학번 : " + num);
		// System.out.println("총점 : " + (kor + math + eng));
		//System.out.println("평균 : " + (kor + math + eng) / 3);
		System.out.println("총점 : " + sum());
		System.out.println("총점 : " + avg());
	}
	
	// 메소드를 따로 빼서 쓰는 것
	int sum() {
		return kor + eng + math;
	}

	double avg() {
		double avgs = sum() / (double) 3;
		return avgs;
	}
}

// 1차!
// 객체 생성시, 두 가지 경우로 필드를 초기화 한다.
// 1) 생성자를 통한 필드 초기화 -> 생성자 (매개변수)
// 2) 객체 필드에 접근하여 필드 초기화 -> 기본 생성자
// 각 필드를 초기화 한 후, getInfo() 메소드를 통하여 위 내용을 출력하시오

// 2차!
//성적 필드에 접근해서 입력
// 학생의 이름 : 김또치
// 학생의 학교 : 예담고등학교
// 학생의 학번 : 221126
// 총 점 : ~~~
// 평 균 : ~~~