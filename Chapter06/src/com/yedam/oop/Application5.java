package com.yedam.oop;

import com.yedam.access.Access;

public class Application5 {
	public static void main(String[] args) {
		Access access = new Access();
		
		// public : 아무데나
		access.free = "free";
		
		// protected : 같은 패키지의 내부에서만
		//access.parent = "parent";
		
		// private : 해당 클래스의 내부에서만 사용 가능
		//access.privacy = "privacy";
		
		// default: 같은 패키지의 내부에서만
		//access.basic = "basic";
	}
}
