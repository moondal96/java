package com.yedam.oop;

public class Car { // 인스턴스 멤버, this
	// 필드
	String model;
	int speed;

	// 생성자
	Car(String model) {
		this.model = model;
	}

	// 메소드
	void setSpeed(int speed) { // @2
		this.speed = speed; // @3
	}

	void run() {
		// 반복문을 만듭니다.
		// 조건은 : i = 10부터 시작하게 해주세요.
		// : i는 50보다 작을 때까지 반복해 주세요.
		// : 반복문을 한번 실행하고 나면 i의 값은 10씩 증가시켜주세요.
		for (int i = 10; i <= 50; i += 10) {
			this.setSpeed(i); // 클래스 내부에서 setSpeed를 쓰는 거임 @1
			System.out.println(this.model + "가 달립니다. (시속 : " + this.speed + "km/h)"); // @4
		}
	}

}
