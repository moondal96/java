package com.yedam.oop;

import java.util.Scanner;

public class Application11 {
	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		int stdCount = 0; // 학생 수 정의
		Student[] stdAry = null; // 학생 배열 정의

		while (true) {
			System.out.println("1. 학생 수 | 2. 학생 정보 입력 | 3. 학생의 총점, 평균 | 4. 종료");
			System.out.println("입력 > ");
			int selectNo = Integer.parseInt(sc.nextLine());

			if (selectNo == 1) {
				System.out.println("학생 수 입력 > ");
				stdCount = Integer.parseInt(sc.nextLine()); // 학생 수 받아오기

			} else if (selectNo == 2) {
				stdAry = new Student[stdCount]; // 배열에 학생 수 받아오기
				for (int i = 0; i < stdAry.length; i++) { // 배열에 정보 넣기
					Student std = new Student(); // 객체 만들기

					// 학생 이름을 입력 받아서 객체에 넣어주기
					System.out.println("학생 이름 > ");
					String name = sc.nextLine();
					std.name = name;

					System.out.println("국어 성적 > ");
					int kor = Integer.parseInt(sc.nextLine());
					std.kor = kor;

					System.out.println("수학 성적 > ");
					std.math = Integer.parseInt(sc.nextLine());

					System.out.println("영어 성적 > ");
					std.eng = Integer.parseInt(sc.nextLine());

					stdAry[i] = std; // i가 0부터 배열의 크기만큼 돌아가는데... 각 방의 정보를 std에 넣어주겠다
				}
			} else if (selectNo == 3) {
				for (int i = 0; i < stdAry.length; i++) {
					System.out.println(stdAry[i].name + " 학생 성적");
					System.out.println("총 점 : " + stdAry[i].sum());
					System.out.println("평 균 : " + stdAry[i].avg());
				}
			} else if (selectNo == 4) {
				System.out.println("프로그램 종료");
				break;
			}
		}
	}
}
