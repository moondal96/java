package com.yedam.oop;

public class ConstantNo { // final 필드
	static final double PI = 3.14159123;
	static final int EARTH_ROUND = 46250;
	static final int LIGHT_SPEED = 127000; // 변하지 않는 값들

	public ConstantNo() {
		// this.pi = 10.0; // final로 지정한 값은 수정 못함
	}
}
