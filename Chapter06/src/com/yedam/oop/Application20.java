package com.yedam.oop;

import com.yedam.access.Access;

public class Application20 {
	
	// import문 : 해당 클래스를 사용하기 위해서 추가해 주는 것
	Access ac = new Access(); // 마우스 오버로 설치
	
	
	// 정적 필드, 주의할 점!!
	int speed; //
	
	void run() {
		System.out.println(speed + "으로 달립니다");
	}
	
	// 메소드 영역에 등록된 것들
	public static void main(String[] args) { // main도 static이넹
//		int speed2 = speed;
//		
//		run(); // main문 밖에서 지정되었기 때문에 안됨
		Application20 app = new Application20();
		app.speed = 5;
		app.run(); // 나 자신을 객체로 만들어야 쓸 수 있음
		
		
		
		
		
		Car myCar = new Car("포르쉐"); // myCar에 설계도 내용을 복사하고
		Car yourCar = new Car("벤츠") ; // yourCar에 설계도 내용을 복사해서
		
		myCar.run();
		yourCar.run(); // 각각 다른 결과를 출력하는 거임
		
		
		
		
		
		// Calculator.java
		// 정적 필드, 메소드를 부르는 방법
		// 정적 멤버가 속해 있는 클래스명.필드 또는 메소드명
		// 1) 정적 필드 가져오는 방법
		double piRatio = Calculator.pi; // 클래스명.필드
		System.out.println(piRatio);
		// 2) 정적 메소드 가져오는 방법
		int result = Calculator.plus(5, 6); // 메소드명
		System.out.println(result);
		
		// 
		// 1. 모든 클래스에서 가져와서 사용할 수 있다 -> 공유의 개념★★★★★
		// 2. 남용해서 사용하면 메모리 누수(부족) 현상이 발생할 수 있음★★★★★
		// 주의할 점!!
		// 정적 메소드에서 외부에 정의한 필드, 메소드를 사용하려고 한다면
		// static이 붙은 필드 또는 메소드만 사용 가능함
		// 만약, static 붙이지 않고 사용하고 싶다면
		// 해당 필드와 메소드가 속해 있는 클래스를 인스턴스화 하여서
		// 인스턴스 필드와 인스턴스 메소드를 dot(.) 연산자를 통해 가져와서 사용해야 함
		
		
		// #Person.java
		Person p1 = new Person("123123-1234567", "김또치");
		System.out.println(p1.nation);
		System.out.println(p1.ssn);
		System.out.println(p1.name);
		
		//p1.nation = "USA"; // final로 지정되었기 떄문에 못 바꿈
		
		//원 넓이
		System.out.println(5*5*ConstantNo.PI);
		
	}
}
