package com.yedam.access;

public class Member { // setter, getter 메소드

	// 필드
	private String id;
	private String pw;
	private String name;
	private int age;

	// 생성자

	// 메소드
	// setter, getter -> 데이터의 무결성을 지키기 위해서
	// setter
	public void setAge(int age) {
		if (age < 0) {
			System.out.println("잘못된 나이입니다.");
			return; // void는 원래 return 안 쓰지만 여기서는 '하던 것을 멈추고
					// 메소드를 종료한 후, 메소드를 호출한 곳(setAge)으로 이동'하게 됨
		} else {
			this.age = age;
		}
		System.out.println("return 적용 안됨");
	}

	// getter
	// 타입을
	public int getAge() {
		// 미국 나이와 한국 나이는 1살 차이 나므로
		// 아래와 같이 실행한다
		age = age + 1;
		return age;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		if (id.length() <= 8) {
			System.out.println("8글자 이하입니다. 다시 입력해주세요");
			return;
		}
		this.id = id;
	}

	public String getPw() {
		return pw;
	}

	public void setPw(String pw) {
		this.pw = pw;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
