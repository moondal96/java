package com.yedam.access;

public class Access { // import문, 접근 제한자
	
	// 접근 제한자
	// public : 어디서든 누구나 다 접근 가능
	// protected : 상속 받은 상태에서 부모-자식 간에 사용(패키지가 달라도 사용 가능)
	// default : 다른 패키지가 사용 못함, 같은 패키지에서만 사용 가능
	// private : 내가 속한 클래스에서만 사용 가능, 정보를 은닉(캡슐화)
	
	// 접근 제한자는 이름을 지어서 사용 : 변수, 클래스, 메소드 등
	
	
	// 필드
	public String free;
	protected String parent;
	private String privacy;
	String basic; // 앞에 아무 것도 없으면 디폴트 타입
	
	// 생성자
	public Access() { //Ctrl + Space바
		
	}
	
	// private는 내부에서만 사용할 수 있는데 생성자를 만드는 게 말이 안됨
	// 그러므로 생성자 앞에는 public만 씀
//	private Access(String privacy) {
//		this.privacy = privacy;
//	}
	
	// 메소드
//	public void run() {
//		System.out.println("달립니다");
//	}
	
	public void free() {
		System.out.println("접근이 가능합니다.");
		privacy();
	}
	
	private void privacy() { // 회사의 특허 기술을 보여주지 않고 사용이 가능하다
		System.out.println("접근이 불가능합니다.");
	}
	
	
	
	
	
	
	
	
	
	
	
	
}
