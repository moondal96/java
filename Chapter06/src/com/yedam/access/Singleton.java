package com.yedam.access;

public class Singleton { // 싱글톤
	
	// 정적 필드
	// 단 하나의 객체를 생성한다(생성자를 private로 막아놨기 때문에 나 자신으로 객체를 만듦)
	private static Singleton singleton = new Singleton();
	
	// 생성자
	private Singleton() {
		
	}
	
	// 정적 메소드
	public static Singleton getInstance() {
		return singleton;
	}

}
