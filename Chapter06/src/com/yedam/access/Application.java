package com.yedam.access;

public class Application {
	public static void main(String[] args) {
		Access access = new Access();
		
		// public : 아무데나
		access.free = "free";
		
		// protected : 같은 패키지의 내부에서만
		access.parent = "parent";
		
		// private : 해당 클래스의 내부에서만 사용 가능
		//access.privacy = "privacy";
		
		// default: 같은 패키지의 내부에서만
		access.basic = "basic";
		
		access.free();
		//access.privacy();
		
		
		// #싱글톤
		Singleton obj1 = Singleton.getInstance();
		Singleton obj2 = Singleton.getInstance();
		// 객체간 주소 비교는 ==
		if(obj1 == obj2) {
			System.out.println("같은 싱글톤 객체입니다");
		} else {
			System.out.println("다른 싱글톤 객체입니다");
		}
	}
}
