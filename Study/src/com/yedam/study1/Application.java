package com.yedam.study1;

import java.util.Scanner;

public class Application {
	public static void main(String[] args) {
		// 두 개의 주사위를 던졌을 때, 눈의 합이 6이 되는 모든 경우의 수를
		// 출력하는 프로그램 구현을 하시오.

		// 아 두 개의 합이 6이 되어야 하구나
		// 1,5
		// 2,4
		// 3,3
		// 4,2
		// 5,1
		// >>중첩 for문

		// A주사위가 1일 때, B주사위의 1~6까지 합을 구한 다음
		// 합의 결과 6이면 -> 내가 원하는 조건이 만족

		for (int i = 1; i <= 6; i++) {
			for (int j = 1; j <= 6; j++) {
				if (i + j == 6) {
					System.out.println("눈의 합이 6이 되는 모든 경우의 수 :" + i + "+" + j);
				}
			}
		}

		// 숫자를 하나 입력 받아, 양수인지 음수인지 출력
		// 단 0이면 0입니다 라고 출력해 주세요
		Scanner sc = new Scanner(System.in);
//		System.out.println("숫자를 입력해 주세요 > ");
//		int num = Integer.parseInt(sc.nextLine()); //문자열을 숫자로 바꿔주는 것
//
//		if (num > 0) {
//			System.out.println("양수입니다");
//		} else if (num < 0) {
//			System.out.println("음수입니다");
//		} else {
//			System.out.println("0입니다");
//		}

		// 정수 두개와 연산기호 1개를 입력 받아서
		// 연산 기호에 해당되는 계산을 수행하고 출력하세요
		System.out.println("정수를 입력해 주세요 : ");
		int num1 = Integer.parseInt(sc.nextLine());
		System.out.println("정수를 입력해 주세요 : ");
		int num2 = Integer.parseInt(sc.nextLine());
		System.out.println("연산기호를 입력해 주세요 : ");
		String sym = sc.nextLine();

		if (sym.equals("+")) { // 문자열을 비교해야 하기 때문에 equals 사용!
			System.out.println(num1 + num2);
		} else if (sym.equals("-")) {
			System.out.println(num1 - num2);
		}
	}
}
