package com.yedam.inter2;

public class Application {
	public static void main(String[] args) {
		Vehicle v1 = new Bus(); // 자동 타입 변환
//		drive(v1);
//
//		Vehicle v2 = new Taxi();
//		drive(v2);

		v1.run();
		// v1.checkFare(); // 자동 타입 변환 : 부모 클래스에 있는 메소드만 사용할 수 있음 > 강제 타입 변환

		Bus bus = (Bus) v1; // 강제 타입 변환

		bus.run();
		bus.checkFare();
		
		drive(new Bus());
		drive(new Taxi());

	}

	public static void drive(Vehicle vehicle) {
		if(vehicle instanceof Bus) { // instanceof : 객체 타입 확인
			Bus bus = (Bus) vehicle;
			bus.checkFare();
		}
		
		vehicle.run();
	}
}
