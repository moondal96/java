package com.yedam.inter2;

public class ImplementC implements InterfaceC {

	@Override
	public void methodA() {
		System.out.println("ImpleC-methodA 실행");

	}

	@Override
	public void methodB() {
		System.out.println("ImpleC-methodB 실행");

	}

	@Override
	public void methodC() {
		System.out.println("ImpleC-methodC 실행");

	}

}
