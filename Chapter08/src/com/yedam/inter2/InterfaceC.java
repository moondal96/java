package com.yedam.inter2;

public interface InterfaceC extends InterfaceA, InterfaceB{
	// Interface A와 Interface B의 내용이 담긴 인터페이스
	// A 기능 + B 기능 + C 기능
	public void methodC();
	
}
