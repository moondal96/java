package com.yedam.inter;

public class Television implements RemoteControl{ // implements : 상속
	
	// 필드
	private int volume;
	
	// 생성자
	
	// 메소드
	@Override
	public void turnOn() {
		System.out.println("전원을 켭니다.");
	}

	@Override
	public void turnOff() {
		System.out.println("전원을 끕니다.");
	}

	@Override
	public void setVolume(int volume) { // 데이터의 무결성을 지켜주기 위함으로 설정
		// 최대 소리 이상으로 데이터가 들어올 때
		if(volume > RemoteControl.MAX_VOLUME) {
			this.volume = RemoteControl.MAX_VOLUME;
		}
		// 최소 소리 이하로 데이터가 들어올 때
		else if(volume < RemoteControl.MIN_VOLUME) {
			this.volume = RemoteControl.MIN_VOLUME;
		}
		else {
			this.volume = volume;
		}
		System.out.println("현재 TV 볼륨 : " + this.volume);
	}

	@Override
	public void search(String url) {
		// TODO Auto-generated method stub
		
	}

}
