package com.yedam.inter;

public class ControllExample {
	public static void main(String[] args) {
		// 인터페이스 구현
		// getInfo 인터페이스
		// area() 메소드 -> 넓이를 구하는 메소드
		// round() 메소드 -> 둘레를 구하는 메소드

		// circle
		// rectangle
		// triangle
		
		GetInfo circle = new Circle(10);
		
		circle.area();
		circle.round();
		
	}
}
