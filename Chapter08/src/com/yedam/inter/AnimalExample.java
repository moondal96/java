package com.yedam.inter;

public class AnimalExample {
	public static void main(String[] args) {
		Animal bird = new Bird();
		
		bird.walk(); // 에니멀에 정의한 메소드를 가져오는 것
		bird.fly();
		bird.sing();
	}
}
