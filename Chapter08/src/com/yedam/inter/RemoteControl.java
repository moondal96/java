package com.yedam.inter;

public interface RemoteControl extends Searchable { // 인터페이스 ##2

	// 상수
	public static final int MAX_VOLUME = 10; // static final 생략 가능
	public int MIN_VOLUME = 0;
	
	// 추상 메소드
	public void turnOn();
	public abstract void turnOff(); // abstract 생략 가능
	public void setVolume(int volume);
	
}
