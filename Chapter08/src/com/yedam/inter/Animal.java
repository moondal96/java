package com.yedam.inter;

public interface Animal { // 인터페이스
	
	void walk();
	void fly();
	void sing();
	
}
