package com.yedam.inter;

public class MyClassExample { // 클래스 내의 인터페이스 사용
	public static void main(String[] args) {
		System.out.println("1)====================");
		
		MyClass myClass = new MyClass(); // 기본 생성자를 통해 myClass 만듦
		
		// 필드 안에 객체를 가지고 있는 경우에는
		// 객체를 담고 있는 클래스를 객체화 시킨 다음,
		// 객체가 품고 있는 필드(rc)의 메소드를 불러서 씀 
		myClass.rc.turnOn(); 
		myClass.rc.turnOn();
		
		System.out.println();
		System.out.println("2)====================");
		
		MyClass myClass2 = new MyClass(new Audio());
		
		System.out.println();
		System.out.println("3)====================");
		
		MyClass myClass3 = new MyClass();
		myClass3.method1();
		
		System.out.println();
		System.out.println("4)====================");
		
		MyClass myClass4 = new MyClass();
		myClass4.methodB(new Television());
		
	}
}
