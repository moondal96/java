package com.yedam.inter;

public class Circle implements GetInfo{

	// 필드 
	// 값을 구해야 하는데 공식이 없어서 필드를 정의해 줘야함
	int radius;
	
	// 생성자
	// 생성자를 통해 데이터를 받아온다
	public Circle(int radius) {
		this.radius = radius;
	}
	
	@Override
	// 원 넓이
	public void area() {
		System.out.println(3.14 * radius * radius);
		
	}

	@Override
	// 원 둘레
	public void round() {
		System.out.println(2 * Math.PI * radius);
		
	}
	
}
