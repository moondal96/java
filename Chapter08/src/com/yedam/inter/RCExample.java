package com.yedam.inter;

public class RCExample { // ##4
	public static void main(String[] args) {
		RemoteControl rc; // 인터페이스는 객체를 못 만들기 때문에 인터페이스에 사용할 수 있게끔 변수를 만든다
		
		rc = new SmartTV();
		
		// SmartTV 클래스 -> implemnets, RemoteControl(+ Searchable)
		// RemoteControl(+ Searchable) -> Searchable을 상속 받고 있기 때문에
		// RemoteControl
		rc.turnOn();
		rc.setVolume(-5);
		rc.turnOff();
		
		// Searchable
		rc.search("www.google.com");
		
//		rc = new Audio(); // 자동 타입 변환
//		
//		rc.turnOn();
//		rc.setVolume(40);
//		rc.turnOff();
		
		
	}
}
